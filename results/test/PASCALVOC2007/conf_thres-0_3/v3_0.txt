{'cfg': 'cfg/voc_yolov3.cfg', 'data': 'data/voc2007.data', 'weights': 'weights/voc_yolov3/size-multi_scale/2020_04_02/17_14_20/best.pt', 'batch_size': 32, 'img_size': 416, 'conf_thres': 0.3, 'iou_thres': 0.6, 'save_json': False, 'task': 'test', 'device': '1', 'single_cls': False, 'mask': False, 'mask_weight': None, 'architecture': 'default', 'working_dir': 'weights/voc_yolov3/size-multi_scale/2020_04_02/17_14_20/'}
Using CUDA device0 _CudaDeviceProperties(name='GeForce GTX 1080 Ti', total_memory=11178MB)

                 all  4.95e+03   1.5e+04     0.919     0.239     0.341     0.365
               Class    Images   Targets         P         R   mAP@0.5        F1
           aeroplane  4.95e+03       311     0.975     0.383     0.468      0.55
             bicycle  4.95e+03       389     0.986     0.182     0.274     0.307
                bird  4.95e+03       576     0.917     0.115     0.208     0.204
                boat  4.95e+03       393     0.984     0.157     0.234     0.271
              bottle  4.95e+03       657     0.819    0.0414    0.0897    0.0787
                 bus  4.95e+03       254     0.954     0.408     0.523     0.572
                 car  4.95e+03  1.54e+03     0.987     0.301     0.401     0.462
                 cat  4.95e+03       370     0.919     0.432     0.555     0.588
               chair  4.95e+03  1.37e+03     0.893    0.0671     0.139     0.125
                 cow  4.95e+03       329     0.939     0.233      0.37     0.374
         diningtable  4.95e+03       299     0.876     0.237      0.33     0.373
                 dog  4.95e+03       530     0.906     0.327     0.444      0.48
               horse  4.95e+03       395      0.95     0.096     0.176     0.174
           motorbike  4.95e+03       369      0.98     0.228     0.338     0.369
              person  4.95e+03  5.23e+03     0.981     0.265     0.447     0.417
         pottedplant  4.95e+03       592     0.804    0.0693     0.152     0.128
               sheep  4.95e+03       311      0.85     0.296      0.39     0.439
                sofa  4.95e+03       396     0.801     0.245     0.303     0.375
               train  4.95e+03       302     0.936     0.435     0.549     0.594
           tvmonitor  4.95e+03       361     0.923     0.265     0.426     0.412
Speed: 5.9/3.5/9.4 ms inference/NMS/total per 416x416 image at batch-size 32
