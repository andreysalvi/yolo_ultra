{'cfg': 'cfg/voc_yolov3.cfg', 'data': 'data/voc2007.data', 'weights': 'weights/voc_yolov3/size-multi_scale/2020_04_09/11_29_14/best.pt', 'batch_size': 32, 'img_size': 416, 'conf_thres': 0.3, 'iou_thres': 0.6, 'save_json': False, 'task': 'test', 'device': '1', 'single_cls': False, 'mask': True, 'mask_weight': None, 'architecture': 'default', 'working_dir': 'weights/voc_yolov3/size-multi_scale/2020_04_09/11_29_14/'}
Using CUDA device0 _CudaDeviceProperties(name='GeForce GTX 1080 Ti', total_memory=11178MB)

Evaluating model with initial weights number of 61573216.0 and final of 6157322.0. 
Reduction of 10.000000953674316%.
                 all  4.95e+03   1.5e+04      0.92     0.255     0.353     0.386
               Class    Images   Targets         P         R   mAP@0.5        F1
           aeroplane  4.95e+03       311     0.975     0.378     0.464     0.545
             bicycle  4.95e+03       389     0.987     0.198     0.274     0.329
                bird  4.95e+03       576      0.93     0.161     0.256     0.274
                boat  4.95e+03       393     0.898     0.179     0.264     0.299
              bottle  4.95e+03       657     0.875    0.0748     0.137     0.138
                 bus  4.95e+03       254     0.923     0.424     0.504     0.581
                 car  4.95e+03  1.54e+03     0.987     0.296     0.401     0.455
                 cat  4.95e+03       370     0.939     0.458     0.604     0.616
               chair  4.95e+03  1.37e+03     0.892    0.0779     0.138     0.143
                 cow  4.95e+03       329     0.919     0.274     0.381     0.422
         diningtable  4.95e+03       299     0.911     0.274     0.373     0.422
                 dog  4.95e+03       530     0.926     0.332     0.443     0.489
               horse  4.95e+03       395     0.944     0.101     0.157     0.183
           motorbike  4.95e+03       369     0.971     0.185     0.345      0.31
              person  4.95e+03  5.23e+03     0.977     0.307     0.455     0.467
         pottedplant  4.95e+03       592     0.824    0.0895     0.179     0.162
               sheep  4.95e+03       311     0.879     0.352     0.429     0.503
                sofa  4.95e+03       396      0.83     0.245     0.311     0.378
               train  4.95e+03       302      0.93     0.437     0.569     0.595
           tvmonitor  4.95e+03       361     0.887      0.26     0.373     0.402
Speed: 5.9/3.3/9.2 ms inference/NMS/total per 416x416 image at batch-size 32
