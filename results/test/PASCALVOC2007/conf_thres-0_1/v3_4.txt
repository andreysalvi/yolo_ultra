{'cfg': 'cfg/voc_yolov3.cfg', 'data': 'data/voc2007.data', 'weights': 'weights/voc_yolov3/size-multi_scale/2020_03_28/09_08_14/best.pt', 'batch_size': 32, 'img_size': 416, 'conf_thres': 0.1, 'iou_thres': 0.6, 'save_json': False, 'task': 'test', 'device': '1', 'single_cls': False, 'mask': False, 'mask_weight': None, 'architecture': 'default', 'working_dir': 'weights/voc_yolov3/size-multi_scale/2020_03_28/09_08_14/'}
Using CUDA device0 _CudaDeviceProperties(name='GeForce GTX 1080 Ti', total_memory=11178MB)

                 all  4.95e+03   1.5e+04     0.919     0.236     0.458      0.36
               Class    Images   Targets         P         R   mAP@0.5        F1
           aeroplane  4.95e+03       311     0.973     0.353     0.568     0.519
             bicycle  4.95e+03       389         1     0.172      0.44     0.294
                bird  4.95e+03       576     0.913     0.153     0.337     0.262
                boat  4.95e+03       393     0.889     0.143     0.305     0.246
              bottle  4.95e+03       657     0.924    0.0558     0.178     0.105
                 bus  4.95e+03       254     0.955     0.414     0.604     0.577
                 car  4.95e+03  1.54e+03     0.993     0.294     0.541     0.453
                 cat  4.95e+03       370     0.904      0.46     0.674      0.61
               chair  4.95e+03  1.37e+03      0.87    0.0586     0.231      0.11
                 cow  4.95e+03       329      0.95     0.229     0.512     0.369
         diningtable  4.95e+03       299     0.897      0.29     0.479     0.439
                 dog  4.95e+03       530     0.901     0.302     0.549     0.452
               horse  4.95e+03       395      0.96    0.0602     0.348     0.113
           motorbike  4.95e+03       369     0.961     0.198     0.496     0.328
              person  4.95e+03  5.23e+03     0.977     0.274     0.597     0.428
         pottedplant  4.95e+03       592     0.788    0.0689     0.262     0.127
               sheep  4.95e+03       311     0.867     0.314     0.513     0.461
                sofa  4.95e+03       396      0.77     0.219     0.373     0.342
               train  4.95e+03       302      0.94     0.418     0.644     0.579
           tvmonitor  4.95e+03       361     0.945     0.236     0.507     0.378
Speed: 5.9/4.1/10.0 ms inference/NMS/total per 416x416 image at batch-size 32
