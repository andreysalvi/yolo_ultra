{'cfg': 'cfg/voc_yolov3_soft_orig-output.cfg', 'data': 'data/voc2007.data', 'weights': 'weights/voc_yolov3_soft_orig-output/size-multi_scale/2020_05_23/18_05_17/best_it_1.pt', 'batch_size': 32, 'img_size': 416, 'conf_thres': 0.001, 'iou_thres': 0.6, 'save_json': False, 'task': 'test', 'device': '3', 'single_cls': False, 'mask': False, 'mask_weight': None, 'architecture': 'default', 'working_dir': 'weights/voc_yolov3_soft_orig-output/size-multi_scale/2020_05_23/18_05_17/'}
Using CUDA device0 _CudaDeviceProperties(name='GeForce GTX 1080 Ti', total_memory=11177MB)

Evaluating model with 61438816.0 parameters removed.
                 all  4.95e+03   1.5e+04     0.877     0.134     0.463     0.223
               Class    Images   Targets         P         R   mAP@0.5        F1
           aeroplane  4.95e+03       311     0.964     0.257     0.563     0.405
             bicycle  4.95e+03       389         1     0.104     0.534     0.188
                bird  4.95e+03       576      0.88    0.0508     0.331     0.096
                boat  4.95e+03       393     0.777     0.135     0.284      0.23
              bottle  4.95e+03       657         1     0.047     0.195    0.0898
                 bus  4.95e+03       254     0.902     0.255     0.536     0.398
                 car  4.95e+03  1.54e+03     0.975     0.201     0.587     0.333
                 cat  4.95e+03       370     0.974     0.199     0.627      0.33
               chair  4.95e+03  1.37e+03     0.778    0.0102     0.213    0.0201
                 cow  4.95e+03       329     0.914     0.161     0.512     0.274
         diningtable  4.95e+03       299     0.842     0.143     0.453     0.244
                 dog  4.95e+03       530     0.975    0.0734     0.557     0.136
               horse  4.95e+03       395     0.678    0.0177     0.517    0.0345
           motorbike  4.95e+03       369     0.889    0.0649     0.558     0.121
              person  4.95e+03  5.23e+03     0.973     0.132     0.633     0.232
         pottedplant  4.95e+03       592     0.672    0.0277     0.213    0.0532
               sheep  4.95e+03       311     0.746     0.245      0.43     0.369
                sofa  4.95e+03       396     0.773     0.147      0.44     0.246
               train  4.95e+03       302     0.911     0.237     0.613     0.376
           tvmonitor  4.95e+03       361     0.908     0.164     0.478     0.279
Speed: 5.9/1.7/7.6 ms inference/NMS/total per 416x416 image at batch-size 32
