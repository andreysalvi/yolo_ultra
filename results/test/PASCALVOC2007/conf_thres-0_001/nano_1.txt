{'cfg': 'cfg/voc_yolov3-nano.cfg', 'data': 'data/voc2007.data', 'weights': 'weights/voc_yolov3_nano/size-multi_scale/2020_04_05/09_07_33/best.pt', 'batch_size': 32, 'img_size': 416, 'conf_thres': 0.001, 'iou_thres': 0.6, 'save_json': False, 'task': 'test', 'device': '1', 'single_cls': False, 'mask': False, 'mask_weight': None, 'architecture': 'default', 'working_dir': 'weights/voc_yolov3_nano/size-multi_scale/2020_04_05/09_07_33/'}
Using CUDA device0 _CudaDeviceProperties(name='GeForce GTX 1080 Ti', total_memory=11178MB)

                 all  4.95e+03   1.5e+04     0.891    0.0449     0.365    0.0825
               Class    Images   Targets         P         R   mAP@0.5        F1
           aeroplane  4.95e+03       311         1    0.0631     0.424     0.119
             bicycle  4.95e+03       389         1    0.0125     0.413    0.0246
                bird  4.95e+03       576     0.883    0.0132     0.194     0.026
                boat  4.95e+03       393     0.715    0.0128      0.15    0.0251
              bottle  4.95e+03       657         1   0.00264    0.0626   0.00528
                 bus  4.95e+03       254     0.977     0.166     0.493     0.284
                 car  4.95e+03  1.54e+03     0.994     0.103     0.469     0.186
                 cat  4.95e+03       370     0.857    0.0595     0.513     0.111
               chair  4.95e+03  1.37e+03     0.559   0.00279     0.127   0.00555
                 cow  4.95e+03       329     0.856    0.0181     0.335    0.0355
         diningtable  4.95e+03       299         1    0.0229     0.435    0.0448
                 dog  4.95e+03       530     0.917    0.0491      0.49    0.0931
               horse  4.95e+03       395     0.474   0.00467     0.453   0.00925
           motorbike  4.95e+03       369     0.985    0.0136     0.512    0.0267
              person  4.95e+03  5.23e+03     0.972    0.0722     0.541     0.134
         pottedplant  4.95e+03       592         1    0.0048     0.112   0.00955
               sheep  4.95e+03       311     0.919      0.11     0.339     0.197
                sofa  4.95e+03       396      0.91    0.0254     0.327    0.0494
               train  4.95e+03       302     0.892    0.0822     0.514     0.151
           tvmonitor  4.95e+03       361     0.916    0.0603     0.403     0.113
Speed: 2.0/4.2/6.3 ms inference/NMS/total per 416x416 image at batch-size 32
