{'cfg': 'cfg/voc_yolov3.cfg', 'data': 'data/voc2007.data', 'weights': 'weights/voc_yolov3/size-multi_scale/2020_03_07/18_43_16/best.pt', 'batch_size': 32, 'img_size': 416, 'conf_thres': 0.001, 'iou_thres': 0.6, 'save_json': False, 'task': 'test', 'device': '1', 'single_cls': False, 'mask': False, 'mask_weight': 'weights/voc_yolov3/size-multi_scale/2020_03_07/18_43_16/mask_1_prune.pt', 'architecture': 'default', 'working_dir': 'weights/voc_yolov3/size-multi_scale/2020_03_07/18_43_16/'}
Using CUDA device0 _CudaDeviceProperties(name='GeForce GTX 1080 Ti', total_memory=11178MB)

Evaluating model with initial weights number of 61573216.0 and final of 6157357.0. 
Reduction of 10.000057220458984%.
                 all  4.95e+03   1.5e+04     0.883     0.323     0.568     0.461
               Class    Images   Targets         P         R   mAP@0.5        F1
           aeroplane  4.95e+03       311      0.96     0.385     0.631      0.55
             bicycle  4.95e+03       389     0.979     0.367     0.662     0.534
                bird  4.95e+03       576     0.933     0.194     0.446     0.321
                boat  4.95e+03       393     0.865     0.186     0.392     0.306
              bottle  4.95e+03       657     0.861    0.0944     0.252      0.17
                 bus  4.95e+03       254       0.9     0.423     0.641     0.575
                 car  4.95e+03  1.54e+03     0.959     0.408     0.686     0.573
                 cat  4.95e+03       370     0.888     0.473     0.718     0.617
               chair  4.95e+03  1.37e+03      0.85     0.112     0.336     0.197
                 cow  4.95e+03       329     0.896     0.287     0.585     0.435
         diningtable  4.95e+03       299     0.841     0.335     0.562     0.479
                 dog  4.95e+03       530     0.874     0.404     0.639     0.553
               horse  4.95e+03       395     0.832     0.389     0.637      0.53
           motorbike  4.95e+03       369     0.909     0.407     0.709     0.562
              person  4.95e+03  5.23e+03     0.955     0.349     0.701     0.512
         pottedplant  4.95e+03       592     0.805     0.117      0.33     0.204
               sheep  4.95e+03       311     0.757      0.33     0.504     0.459
                sofa  4.95e+03       396     0.793     0.338     0.568     0.474
               train  4.95e+03       302     0.924     0.497     0.732     0.646
           tvmonitor  4.95e+03       361     0.872     0.366      0.62     0.515
Speed: 5.8/4.1/9.9 ms inference/NMS/total per 416x416 image at batch-size 32
