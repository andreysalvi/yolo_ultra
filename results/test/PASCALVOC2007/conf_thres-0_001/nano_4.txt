{'cfg': 'cfg/voc_yolov3-nano.cfg', 'data': 'data/voc2007.data', 'weights': 'weights/voc_yolov3_nano/size-multi_scale/2020_04_09/05_49_30/best.pt', 'batch_size': 32, 'img_size': 416, 'conf_thres': 0.001, 'iou_thres': 0.6, 'save_json': False, 'task': 'test', 'device': '1', 'single_cls': False, 'mask': False, 'mask_weight': None, 'architecture': 'default', 'working_dir': 'weights/voc_yolov3_nano/size-multi_scale/2020_04_09/05_49_30/'}
Using CUDA device0 _CudaDeviceProperties(name='GeForce GTX 1080 Ti', total_memory=11178MB)

                 all  4.95e+03   1.5e+04     0.941    0.0383     0.382    0.0713
               Class    Images   Targets         P         R   mAP@0.5        F1
           aeroplane  4.95e+03       311         1    0.0519      0.47    0.0987
             bicycle  4.95e+03       389         1   0.00627     0.435    0.0125
                bird  4.95e+03       576         1   0.00335     0.208   0.00668
                boat  4.95e+03       393     0.762    0.0164     0.156    0.0321
              bottle  4.95e+03       657         1   0.00292    0.0681   0.00583
                 bus  4.95e+03       254     0.973     0.144      0.52     0.251
                 car  4.95e+03  1.54e+03         1    0.0892     0.488     0.164
                 cat  4.95e+03       370     0.888    0.0214      0.53    0.0418
               chair  4.95e+03  1.37e+03         1     0.001     0.158     0.002
                 cow  4.95e+03       329     0.819    0.0274     0.368    0.0529
         diningtable  4.95e+03       299     0.921    0.0389     0.459    0.0747
                 dog  4.95e+03       530     0.929    0.0493     0.496    0.0936
               horse  4.95e+03       395         1   0.00253     0.524   0.00505
           motorbike  4.95e+03       369     0.864    0.0173     0.504    0.0339
              person  4.95e+03  5.23e+03      0.97     0.068     0.548     0.127
         pottedplant  4.95e+03       592         1   0.00169    0.0972   0.00337
               sheep  4.95e+03       311     0.885    0.0493     0.345    0.0935
                sofa  4.95e+03       396         1    0.0238     0.357    0.0465
               train  4.95e+03       302     0.888    0.0789     0.512     0.145
           tvmonitor  4.95e+03       361      0.93    0.0733     0.392     0.136
Speed: 2.1/4.1/6.1 ms inference/NMS/total per 416x416 image at batch-size 32
